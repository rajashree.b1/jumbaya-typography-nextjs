import Head from 'next/head';

// import typography css
import styles from "../styles/Typography.module.css";

export default function Home(){
  return(
    <div>

        <Head>
            <title>Jumbaya Typography</title>
            <link rel="preconnect" href="https://fonts.googleapis.com" />
            <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />

            <link 
            href="https://fonts.googleapis.com/css2?family=Baloo+2:wght@400;500;600;700;800&display=swap" 
            rel="stylesheet" /> 

        </Head>
        <h1 className='underline uppercase mt_5 textcenter Primary_1'>Typography</h1>
        <div className={styles.typo_table}>
            <div className={styles.typo_dataTable}>
                <table className={styles.typo_dataTable__table}>
                    <thead>
                        <tr className={styles.typo_dataTable__headerrow}>
                            <th className={` ${styles.typo_dataTable__headerCell} Primary_bg_color white_color col-3`}>Scale</th>
                            <th className={` ${styles.typo_dataTable__headerCell} Primary_bg_color white_color`} >Properties</th>
                        </tr>
                    </thead>
                    <tbody className={styles.typo_dataTable__content}>
                        <tr className={styles.typo_dataTable__row}>
                            <td className={styles.typo_dataTable__cell}>
                                <span className="display_text1">Display 1</span>
                            </td>
                            <td className={styles.typo_dataTable__cell}>
                                font-family: 'Baloo 2';<br></br>
                                font-weight: 700;<br></br>
                                font-size: 64px;<br></br>
                                line-height: 103px;<br></br>
                                color: #000000;<br></br>
                            </td>
                        </tr>
                        <tr className={styles.typo_dataTable__row}>
                            <td className={styles.typo_dataTable__cell}>
                                <span className="display_text2">Display 2</span>
                            </td>
                            <td className={styles.typo_dataTable__cell}>
                                font-family: 'Baloo 2';<br></br>
                                font-weight: 400;<br></br>
                                font-size: 56px;<br></br>
                                line-height: 90px;<br></br>
                                color: #000000;<br></br>
                            </td>
                        </tr>
                        <tr className={styles.typo_dataTable__row}>
                            <td className={styles.typo_dataTable__cell}>
                                <h1>Headline 1</h1>
                            </td>
                            <td className={styles.typo_dataTable__cell}>
                                font-family: 'Baloo 2';<br></br>
                                font-size: 28px;<br></br>
                                line-height: 36px;<br></br>
                                font-weight: 700;<br></br>
                            </td>
                        </tr>
                        <tr className={styles.typo_dataTable__row}>
                            <td className={styles.typo_dataTable__cell}>
                                <h2>Headline 2</h2>
                            </td>
                            <td className={styles.typo_dataTable__cell}>
                                font-family: 'Baloo 2';<br></br>
                                font-weight: 700;<br></br>
                                font-size: 24px;<br></br>
                                line-height: 32px;<br></br>
                            </td>
                        </tr>
                        <tr className={styles.typo_dataTable__row}>
                            <td className={styles.typo_dataTable__cell}>
                                <h3>Headline 3</h3>
                            </td>
                            <td className={styles.typo_dataTable__cell}>
                                font-family: 'Baloo 2';<br></br>
                                font-weight: 700;<br></br>
                                font-size: 20px;<br></br>
                                line-height: 26px;<br></br>
                            </td>
                        </tr>
                        <tr className={styles.typo_dataTable__row}>
                            <td className={styles.typo_dataTable__cell}>
                                <h4>Headline 4</h4>
                            </td>
                            <td className={styles.typo_dataTable__cell}>
                                font-family: 'Baloo 2';<br></br>
                                font-weight: 600;<br></br>
                                font-size: 16px;<br></br>
                                line-height: 22px;<br></br>
                            </td>
                        </tr>
                        <tr className={styles.typo_dataTable__row}>
                            <td className={styles.typo_dataTable__cell}>
                                <span className="body1">BodyText 1</span>
                            </td>
                            <td className={styles.typo_dataTable__cell}>
                                font-family: 'Baloo 2';<br></br>
                                font-weight: 400;<br></br>
                                font-size: 18px;<br></br>
                                line-height: 24px;<br></br>
                                color: #000000;<br></br>
                            </td>
                        </tr>
                        <tr className={styles.typo_dataTable__row}>
                            <td className={styles.typo_dataTable__cell}>
                                <span className="body2">BodyText 2</span>
                            </td><td className={styles.typo_dataTable__cell}>
                                font-family: 'Baloo 2';<br></br>
                                font-weight: 500;<br></br>
                                font-size: 14px;<br></br>
                                line-height: 20px;<br></br>
                                color: #000000;<br></br>
                            </td>
                        </tr>
                        <tr className={styles.typo_dataTable__row}>
                            <td className={styles.typo_dataTable__cell}>
                                <span className="small">Small Text</span>
                            </td>
                            <td className={styles.typo_dataTable__cell}>
                                font-family: 'Baloo 2';<br></br>
                                font-weight: 400;<br></br>
                                font-size: 10px;<br></br>
                                line-height: 16px;<br></br>
                                color: #000000;<br></br>
                            </td>
                        </tr>
                        <tr className={styles.typo_dataTable__row}>
                            <td className={styles.typo_dataTable__cell}>
                                <span className="indi_msgs">Indicator Messages</span>
                            </td>
                            <td className={styles.typo_dataTable__cell}>
                                font-family: 'Baloo 2';<br></br>
                                font-weight: 500;<br></br>
                                font-size: 14px;<br></br>
                                line-height: 18px;<br></br>
                                color: #000000;<br></br>
                            </td>
                        </tr>
                        <tr className={styles.typo_dataTable__row}>
                            <td className={styles.typo_dataTable__cell}>
                                <button className={` ${styles.submit_btn} button`} type="button">Button</button>
                            </td>
                            <td className={styles.typo_dataTable__cell}>
                                display: flex;<br></br>
                                flex-direction: row;<br></br>
                                justify-content: center;<br></br>
                                align-items: center;<br></br>
                                padding: 16px;<br></br>
                                width: 280px;<br></br>
                                height: 48px;<br></br>
                                background-color: #953389;<br></br>
                                box-shadow: 0px 4px 0px rgba(0, 0, 0, 0.12), inset 0px -4px 0px rgba(0, 0, 0, 0.25);<br></br>
                                border-radius: 16px;<br></br>
                                color: #ffffff;<br></br>
                                border: 0;<br></br>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        
        {/* <div className={styles.typography}>
            <div className="c_container">
                <div className="c_row mb_5">
                    <h4 className="underline m_auto Dark_color w_100">Display 1 :</h4>
                    <h1 className="m_auto w_100 display_text1">Hello
                        <span className="display_text2 Primary_1">Jumbaya</span>
                    </h1>
                </div>
                <div className="c_row">
                    <h4 className="underline m_auto Dark_color w_100">Heading 1 :</h4>
                    <h1 className="m_auto w_100">Jumbaya</h1>

                    <h4 className="underline m_auto Dark_color w_100">Heading 2 :</h4>
                    <h2 className="m_auto Primary_1 w_100">Verification</h2>

                    <h4 className="underline m_auto Dark_color w_100">Heading 3 :</h4>
                    <h3 className="m_auto Primary_1 w_100">Verification 1</h3>

                    <h4 className="underline m_auto Dark_color w_100">Heading 4 :</h4>
                    <h4 className="m_auto Primary_1 w_100">Verification 2</h4>

                    <h4 className="underline m_auto Dark_color w_100">Body 1 :</h4>
                    <p className="body1 mb_5">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat sunt mollitia, doloremque consectetur officia molestiae aut quas doloribus hic sapiente sit rerum consequatur quis laboriosam quibusdam ad repudiandae voluptate autem.</p>

                    <h4 className="underline m_auto Dark_color w_100">Body 2 :</h4>
                    <p className="m_auto body2 w_100 mb_5">Lorem ipsum dolor</p>

                    <h4 className="underline m_auto Dark_color w_100">Text Styles Button :</h4>
                    <button className={styles.submitbtn} type="button">Next</button>

                    <h4 className="underline m_auto mt_5 Dark_color w_100">Text Styles Small :</h4>
                    <p className="small w_100">Lorem ipsum dolor</p>

                    <h4 className="underline m_auto Dark_color w_100">Text Styles Indicator Messages :</h4>
                    <p className="indi_msgs w_100">Lorem ipsum dolor</p>
                </div>
            </div>
        </div> */}
    </div>
  )
}
